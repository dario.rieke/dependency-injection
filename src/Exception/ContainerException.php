<?php 

namespace DarioRieke\DependencyInjection\Exception;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends \Exception implements ContainerExceptionInterface {

}