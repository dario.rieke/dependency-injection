<?php 
namespace DarioRieke\DependencyInjection;

use DarioRieke\DependencyInjection\Exception\ContainerException;
use DarioRieke\DependencyInjection\Exception\NotFoundException;

/**
 * DependencyInjectionContainer
 */
class DependencyInjectionContainer implements DependencyInjectionContainerInterface {

	/**
	 * associative array of parameters
	 * @var array
	 */
	private $parameters = [];

	/**
	 * array of singletons 
	 * singletons creation callable is only called once and then cached
	 * @var array
	 */
	private $singletons = [];

	/**
	 * array of singletons which got called at least once
	 * the container will return a singleton from the cache if it has already been instantiated
	 * @var array
	 */
	private $singletonsCache = [];

	/**
	 * array of factories 
	 * factory callables get called each time a dependency is requested
	 * @var array
	 */
	private $factories = [];

	/**
	 * aliases for the dependency names
	 * [
	 * 		'\Example\AliasInterface' => 'dependencyName'
	 * ]
	 * @var array
	 */
	private $aliases = [];

	/**
	 * methodCalls to run after dependency has been created
	 * @var array
	 * [
	 * 		'dependencyName' => [ 
	 * 			'methodName1' => ['argument1', 'argument2', 3],
	 * 			'methodName2' => ['argument1-1', 'argument2-2', 3.3],
	 * 			
	 * 			
	 * 		 ]
	 * ]
	 */
	private $methodCalls = [];

	public function singleton(string $name, callable $func) {
		$this->singletons[$name] = $func;
	}

	public function register(string $name, callable $func) {
		$this->factories[$name] = $func;
	}

	public function get($name) {
		if(!is_string($name)) throw new ContainerException("Passed in identifier must be of type string.");
		
		if($dependency = $this->getByAlias($name)) {
			return $dependency;
		}
		elseif($dependency = $this->getByName($name)) {
			return $dependency;
		}
		else {
			throw new NotFoundException("Dependency $name not found.");
		}
	}

	public function alias(string $dependencyName, string $aliasName) {
		$this->aliases[$aliasName] = $dependencyName;
	}

	/**
	 * get a dependency by name
	 * @param string $name   name of the dependency 
	 * @return mixed		 dependency or null
	 */
	protected function getByName(string $name) {

		if(array_key_exists($name, $this->singletonsCache)) {
			$dependency = $this->singletonsCache[$name];
			//return dependency instantly and ingnore additional method calls
			return $dependency;
		}
		elseif(array_key_exists($name, $this->singletons)) {
			$singleton = call_user_func_array($this->singletons[$name], [$this]);
			$this->singletonsCache[$name] = $singleton;
			$dependency = $singleton;
		}
		elseif(array_key_exists($name, $this->factories)) {
			$dependency = call_user_func_array($this->factories[$name], [$this]); 
		}
		else {
			$dependency = null;
		}
		//check for additional method calls to run
		if(array_key_exists($name, $this->methodCalls)) {
			$calls = $this->methodCalls[$name];
			foreach ($calls as $method => $arguments) {
				if(is_callable([$dependency, $method])) {
					call_user_func_array([$dependency, $method], $arguments);
				}
				else {
					throw new ContainerException("Method $method does not exist at dependency $name");
				}
			}
		}

		return $dependency;
	}

	/**
	 * get a dependency by its alias
	 * @param string $bindingName name of the alias 
	 * @return mixed			  dependency or null
	 */
	protected function getByAlias(string $bindingName) {
		if($this->isAlias($bindingName)) {
			return $this->getByName($this->aliases[$bindingName]);
		}
		else {
			return null;
		}
	}

	/**
	 * 	check if the given name is an alias
	 * @param  string  $name name of the alias
	 * @return boolean       
	 */
	protected function isAlias(string $name) {
		return array_key_exists($name, $this->aliases);
	}

	public function setParameter(string $name, $value) {
		$this->parameters[$name] = $value;
	}


	public function getParameter(string $name) {
		if (array_key_exists($name, $this->parameters)) {
			return $this->parameters[$name];
		}
		else {
			throw new NotFoundException("Parameter $name not found.");
		}
	}

	public function hasParameter(string $name): bool {
		return array_key_exists($name, $this->parameters);
	}

	public function has($name) {
		if(!is_string($name)) throw new ContainerException("Passed in identifier must be of type string.");
		
		return (array_key_exists($name, $this->factories) || array_key_exists($name, $this->singletons) || $this->isAlias($name));
	}

	/**
	 * 	helper to get the dependency name by alias
	 * @param  string $alias name of the alias
	 */
	protected function getDependencyNameByAlias(string $alias) {
		if($this->aliases[$alias]) return $this->aliases[$alias];
	}

	public function addMethodCall(string $name, string $method, array $arguments) {
		if($this->has($name)) {
			if($this->isAlias($name)) {
				$name = $this->getDependencyNameByAlias();
			}
			//if the dependency is a singleton and the singleton has already been constructed, you cant add method calls afterwards to it as it always returns the same instance
			if(array_key_exists($name, $this->singletonsCache)) {
				throw new ContainerException("You can not add a call to a method to an already constructed singleton as always the same instance is returned.");
				
			}
			$this->methodCalls[$name][$method] = $arguments;
		}
		else {
			throw new NotFoundException("Dependency $name not found.");
		}
	}
}

?>
