<?php 

namespace DarioRieke\DependencyInjection\Exception;

use Psr\Container\NotFoundExceptionInterface;


class NotFoundException extends \Exception implements NotFoundExceptionInterface {
	
}